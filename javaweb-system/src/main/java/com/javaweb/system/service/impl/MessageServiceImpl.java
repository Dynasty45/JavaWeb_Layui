// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2023 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed LGPL-3.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.javaweb.common.common.BaseQuery;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.common.common.BaseServiceImpl;
import com.javaweb.system.constant.MessageConstant;
import com.javaweb.system.entity.Message;
import com.javaweb.system.mapper.MessageMapper;
import com.javaweb.system.query.MessageQuery;
import com.javaweb.system.service.IMessageService;
import com.javaweb.system.utils.ShiroUtils;
import com.javaweb.system.utils.UserUtils;
import com.javaweb.system.vo.message.MessageListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 系统消息 服务实现类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-05-04
 */
@Service
public class MessageServiceImpl extends BaseServiceImpl<MessageMapper, Message> implements IMessageService {

    @Autowired
    private MessageMapper messageMapper;

    /**
     * 获取数据列表
     *
     * @param query 查询条件
     * @return
     */
    @Override
    public JsonResult getList(BaseQuery query) {
        MessageQuery messageQuery = (MessageQuery) query;
        // 查询条件
        QueryWrapper<Message> queryWrapper = new QueryWrapper<>();
        // 消息标题
        if (!StringUtils.isEmpty(messageQuery.getTitle())) {
            queryWrapper.like("title", messageQuery.getTitle());
        }
        // 发送方式：1系统 2短信 3邮件 4微信 5推送
        if (messageQuery.getType() != null && messageQuery.getType() > 0) {
            queryWrapper.eq("type", messageQuery.getType());
        }
        // 发送状态：1已发送 2未发送
        if (messageQuery.getSendStatus() != null && messageQuery.getSendStatus() > 0) {
            queryWrapper.eq("send_status", messageQuery.getSendStatus());
        }
        queryWrapper.eq("mark", 1);
        queryWrapper.orderByDesc("id");

        // 查询数据
        IPage<Message> page = new Page<>(messageQuery.getPage(), messageQuery.getLimit());
        IPage<Message> data = messageMapper.selectPage(page, queryWrapper);
        List<Message> messageList = data.getRecords();
        List<MessageListVo> messageListVoList = new ArrayList<>();
        if (!messageList.isEmpty()) {
            messageList.forEach(item -> {
                MessageListVo messageListVo = new MessageListVo();
                // 拷贝属性
                BeanUtils.copyProperties(item, messageListVo);
                // 发送方式描述
                if (messageListVo.getType() != null && messageListVo.getType() > 0) {
                    messageListVo.setTypeName(MessageConstant.MESSAGE_TYPE_LIST.get(messageListVo.getType()));
                }
                // 发送状态描述
                if (messageListVo.getSendStatus() != null && messageListVo.getSendStatus() > 0) {
                    messageListVo.setSendStatusName(MessageConstant.MESSAGE_SENDSTATUS_LIST.get(messageListVo.getSendStatus()));
                }
                // 添加人名称
                if (messageListVo.getCreateUser() > 0) {
                    messageListVo.setCreateUserName(UserUtils.getName((messageListVo.getCreateUser())));
                }
                // 更新人名称
                if (messageListVo.getUpdateUser() > 0) {
                    messageListVo.setUpdateUserName(UserUtils.getName((messageListVo.getUpdateUser())));
                }
                messageListVoList.add(messageListVo);
            });
        }
        return JsonResult.success("操作成功", messageListVoList, data.getTotal());
    }

    /**
     * 添加或编辑
     *
     * @param entity 实体对象
     * @return
     */
    @Override
    public JsonResult edit(Message entity) {
        if (StringUtils.isNotNull(entity.getId()) && entity.getId() > 0) {
            entity.setUpdateUser(ShiroUtils.getUserId());
        } else {
            entity.setCreateUser(ShiroUtils.getUserId());
        }
        return super.edit(entity);
    }

    /**
     * 删除记录
     *
     * @param id 记录ID
     * @return
     */
    @Override
    public JsonResult deleteById(Integer id) {
        if (id == null || id == 0) {
            return JsonResult.error("记录ID不能为空");
        }
        Message entity = this.getById(id);
        if (entity == null) {
            return JsonResult.error("记录不存在");
        }
        return super.delete(entity);
    }

}